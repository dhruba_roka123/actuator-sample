package com.bijay.actuatorsample.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bijay.actuatorsample.model.Topic;

@Repository
public interface TopicRepository extends CrudRepository<Topic, Long> {
	
}
