package com.bijay.actuatorsample.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bijay.actuatorsample.model.Course;

@Repository
public interface CourseRepository extends CrudRepository<Course, Long>{
	
	public List<Course> findByTopicId(Long topicId);

}
