package com.bijay.actuatorsample.service;

import java.util.List;
import java.util.Optional;

import com.bijay.actuatorsample.model.Topic;

public interface TopicService {
	
	public List<Topic> getAllTopics();
	
	public Optional<Topic> getTopic(Long id);
	
	public void addTopic(Topic topic);
	
	public void updateTopic(Topic topic, Long id);
	
	public void deleteTopic(Long id);

}
