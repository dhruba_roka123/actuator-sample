package com.bijay.actuatorsample.service;

import java.util.List;
import java.util.Optional;

import com.bijay.actuatorsample.model.Course;

public interface CourseService {
	
	public List<Course> getAllCourses(Long topicId);
	
	public Optional<Course> getCourse(Long id);
	
	public void addCourse(Course course);
	
	public void updateCourse(Course course);
	
	public void deleteCourse(Long id);
	
}
