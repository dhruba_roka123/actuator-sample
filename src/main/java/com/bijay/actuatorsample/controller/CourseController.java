package com.bijay.actuatorsample.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bijay.actuatorsample.model.Course;
import com.bijay.actuatorsample.model.Topic;
import com.bijay.actuatorsample.service.CourseService;

@RestController
@RequestMapping(value = "/api/topics/{topicId}")
public class CourseController {
	
	@Autowired
	private CourseService courseService;
	
	@RequestMapping(value = "/courses", method = RequestMethod.GET)
	public List<Course> getAllCourses(@PathVariable Long topicId) {
		return courseService.getAllCourses(topicId);
	}
	
	@RequestMapping(value = "/courses/{id}", method = RequestMethod.GET)
	public Optional<Course> getCourseDetails(@PathVariable Long id) {
		return courseService.getCourse(id);
	}
	
	@RequestMapping(value = "/courses", method = RequestMethod.POST)
	public String addCourse(@RequestBody Course course, @PathVariable Long topicId){
		course.setTopic(new Topic(topicId, "", ""));
		courseService.addCourse(course);
		return "Course added Successfully !!";
	}
	
	@RequestMapping(value = "/courses/{id}", method = RequestMethod.PUT)
	public String updateCourse(@RequestBody Course course, @PathVariable Long topicId, @PathVariable Long id) {
		course.setTopic(new Topic(topicId, "", ""));
		courseService.updateCourse(course);
		return "Course has been updated Successfully !!";
	}
	
	@RequestMapping(value = "/courses/{id}", method = RequestMethod.DELETE)
	public String deleteCourse(@PathVariable Long id) {
		courseService.deleteCourse(id);
		return "Course deleteed Successfully !!";
	}

}
