package com.bijay.actuatorsample.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bijay.actuatorsample.model.Topic;
import com.bijay.actuatorsample.service.TopicService;

@RestController
public class TopicController {
	
	@Autowired
	private TopicService topicService;

	// private Topic topicObj;
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public String showTopics() {
		return "GitLab CICD Project !!";
	}
	
	@RequestMapping(value = "/api/topics", method = RequestMethod.GET)
	public List<Topic> getAllTopics(){
		return topicService.getAllTopics();
	}
	
	@RequestMapping(value = "/api/topics/{id}", method = RequestMethod.GET)
	public Optional<Topic> getTopicDetails(@PathVariable Long id) {
		return topicService.getTopic(id);
	}

	@RequestMapping(value = "/api/topics", method = RequestMethod.POST)
	public String addTopic(@RequestBody Topic topic){
		topicService.addTopic(topic);
		return "Topic Saved Successfully.";
	}
	
	@RequestMapping(value = "/api/topics/{id}", method = RequestMethod.PUT)
	public String updateTopic(@RequestBody Topic topic, @PathVariable Long id) {
		topicService.updateTopic(topic, id);
		return "Successfully Updated Topic.";
	}
	
	@RequestMapping(value = "/api/topics/{id}", method = RequestMethod.DELETE)
	public String deleteTopic(@PathVariable Long id) {
		topicService.deleteTopic(id);
		return "Successfully Deleted Topic.";
	}

}
